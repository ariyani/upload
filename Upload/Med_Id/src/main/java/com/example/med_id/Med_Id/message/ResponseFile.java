package com.example.med_id.Med_Id.message;

public class ResponseFile {
    private String imagePath;
    private String url;
    private String type;
    private long size;

    public ResponseFile(String imagePath, String url, String type, long size) {
        this.imagePath = imagePath;
        this.url = url;
        this.type = type;
        this.size = size;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}


