package com.example.med_id.Med_Id.models;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "files")
public class Biodata {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")


    @Column(name = "id", nullable = false)
    private String Id;

//    @Column(name = "fullname")
//    private String Fullname;
//
//    @Column(name = "mobile_phone", length = 15, nullable = true)
//    private String MobilePhone;

    @Column(name = "image_path")
    private String ImagePath;

    @Column(name = "type")
    private String Type;

    @Lob
    @Column(name = "image")
    private byte[] Image;

    public Biodata() {
    }

    public Biodata(String imagePath, String type, byte[] image) {
        this.ImagePath = imagePath;
        this.Type = type;
        this.Image = image;
    }


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public byte[] getImage() {
        return Image;
    }

    public void setImage(byte[] image) {
        Image = image;
    }
}