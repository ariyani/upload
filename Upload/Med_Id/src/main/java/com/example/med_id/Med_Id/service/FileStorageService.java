package com.example.med_id.Med_Id.service;



import com.example.med_id.Med_Id.models.Biodata;
import com.example.med_id.Med_Id.repository.BiodataRepo;
import javafx.scene.canvas.GraphicsContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.stream.Stream;

@Service
public class FileStorageService {


    @Autowired
    private static BiodataRepo biodataRepo;

    public static Biodata store(MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        Biodata Biodata = new Biodata(fileName, file.getContentType(), file.getBytes());

        return biodataRepo.save(Biodata);
    }
    public Biodata getFile(String id) {return biodataRepo.findById(id).get();}

    public Stream<Biodata> getAllFiles(){ return biodataRepo.findAll().stream();}

}
